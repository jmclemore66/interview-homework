# README #

Have Java11 and maven installed and you should be able to just run [mvn run spring-boot:run].
If that gives you trouble compile with [mvn clean install] and try again.
### What is this repository for? ###

* Availity interview homework

### How do I get set up? ###

* Use the run command above to run the project.
* Question number 4. You can make a POST with a body of a raw string to http://localhost:1991/lisp/validate 
* to validate lisp parenthesis. It will return a true or false if the code contains parenthesis
* and they correctly open and close or not. You can also run the project tests to see this working.
* Question number 6. To test the CSV parser run the project and make a GET http://localhost:1991/csv/parse. 
* csv files should be in the src/main/resources/csv/results/ folder, they will overwrite if you 
* run multiple times and you can delete them and they will regenerate.


### Who do I talk to? ###

* Project owner Jordan McLemore