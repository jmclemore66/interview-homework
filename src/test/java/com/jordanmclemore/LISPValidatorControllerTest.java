package com.jordanmclemore;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LISPValidatorControllerTest {

  private final static String LISP_VALIDATE_REQUEST = "/lisp/validate";

  private final static String LISP_CODE_VALID_EXAMPLE =
      "(defun demo-function (flag)"
          + "   (print 'entering-outer-block)"
          + "   (block outer-block"
          + "      (print 'entering-inner-block)"
          + "      (print (block inner-block"
          + "         (if flag"
          + "            (return-from outer-block 3)"
          + "            (return-from inner-block 5)"
          + "         )"
          + "         (print 'This-wil--not-be-printed))"
          + "      )"
          + "      (print 'left-inner-block)"
          + "      (print 'leaving-outer-block)"
          + "   t)"
          + ")"
          + "(demo-function t)"
          + "(terpri)"
          + "(demo-function nil)";

  private final static String LISP_CODE_INVALID_EXAMPLE =
      "(defun factorial (n &optional (acc 1))"
          + "    (if (zerop n) acc"
          + "        (factorial (1- n) (* acc n)";

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void testLISPValidatorController_andReturnTrue() throws Exception {
    mockMvc.perform(post(LISP_VALIDATE_REQUEST)
        .content(LISP_CODE_VALID_EXAMPLE))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("true"));
  }

  @Test
  public void testLISPValidatorController_andReturnFalse() throws Exception {
    mockMvc.perform(post(LISP_VALIDATE_REQUEST)
            .content(LISP_CODE_INVALID_EXAMPLE))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("false"));
  }

  @Test
  public void testLISPValidatorController_withEmptyRequest() throws Exception {
    mockMvc.perform(post(LISP_VALIDATE_REQUEST)
            .content(" "))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testLISPValidatorController_withBadRequest() throws Exception {
    mockMvc.perform(post(LISP_VALIDATE_REQUEST))
        .andExpect(status().isBadRequest());
  }

}
