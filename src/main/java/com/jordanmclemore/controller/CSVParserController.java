package com.jordanmclemore.controller;

import com.jordanmclemore.service.CSVParserOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("csv")
public class CSVParserController {

  @Autowired
  private CSVParserOperations CSVParserService;

  @GetMapping("/parse")
  public ResponseEntity<String> parseCSV() {
    CSVParserService.parseCSV();
    return ResponseEntity.ok().body("success");
  }
}
