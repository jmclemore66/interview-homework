package com.jordanmclemore.controller;

import com.jordanmclemore.service.LispValidatorOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("lisp")
public class LISPValidatorController
{

  @Autowired
  private LispValidatorOperations lispValidatorService;

  @PostMapping("/validate")
  public ResponseEntity<Boolean> validateListParenthesis(@RequestBody String lispCode) {
    if(!StringUtils.hasText(lispCode)) {
      return new ResponseEntity("Empty request", HttpStatus.BAD_REQUEST);
    }
    return ResponseEntity.ok().body(lispValidatorService.hasValidParenthesis(lispCode));
  }
}
