package com.jordanmclemore.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PatientModel {
  private String userId;
  private String firstName;
  private String lastName;
  private Integer version;
  private String insuranceCompany;
}
