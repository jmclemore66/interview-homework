package com.jordanmclemore.service;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class LISPValidatorService implements LispValidatorOperations {

  /**
   * This method checks if the list code correctly closes all open parenthesis
   * @param lispCode code input
   * @return boolean
   */
  public boolean hasValidParenthesis(String lispCode) {
    String lispWithoutStrings = removeCodeStrings(lispCode);
    int openParenthesisCount = StringUtils.countOccurrencesOf(lispWithoutStrings, "(");
    int closeParenthesisCount = StringUtils.countOccurrencesOf(lispWithoutStrings, ")");
    return openParenthesisCount == closeParenthesisCount;
  }

  /**
   * Removing all instances of Strings from the code
   * @param lispCode code input
   * @return boolean
   */
  private String removeCodeStrings(String lispCode) {
    return lispCode.replaceAll("\"(?:[^\"\\\\]|\\\\.)*\"", "");
  }
}
