package com.jordanmclemore.service;

public interface LispValidatorOperations {

  boolean hasValidParenthesis(String lispCode);

}
