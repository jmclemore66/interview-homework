package com.jordanmclemore.service;

import com.jordanmclemore.model.PatientModel;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CSVParserService implements CSVParserOperations {

  private static final String EXAMPLE_CSV_PATH = "src/main/resources/csv/example.csv";
  private static final String CSV_RESULTS_PATH = "src/main/resources/csv/results/";
  private static final int USER_ID_INDEX = 0;
  private static final int FIRST_NAME_INDEX = 1;
  private static final int LAST_NAME_INDEX = 2;
  private static final int VERSION_INDEX = 3;
  private static final int COMPANY_INDEX = 4;

  /**
   * Reads an example csv file, parses that files, and generates a csv file for each company
   */
  public void parseCSV() {

    List<PatientModel> patientModels = readPatientsFromCsv();
    Map<String, List<PatientModel>> patientCompanyMap = groupPatientsByCompany(patientModels);
    sortByLastAndFirstName(patientCompanyMap);
    createCSVFilesForEachCompany(patientCompanyMap);
  }

  /**
   * Read all lines from the patient example csv and populate a list of models
   * @return List<PatientModel> patientModels list of all patients from the csv imports
   */
  private List<PatientModel> readPatientsFromCsv() {
    Path pathToFile = Paths.get(EXAMPLE_CSV_PATH);

    List<PatientModel> patients = new ArrayList<>(); //result model
    try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) { //read file
      String line = br.readLine();
      while (line != null) {
        String[] patientArr = line.split(",");
        PatientModel patientModel = new PatientModel(patientArr[USER_ID_INDEX],
            patientArr[FIRST_NAME_INDEX], patientArr[LAST_NAME_INDEX],
            Integer.valueOf(patientArr[VERSION_INDEX]), patientArr[COMPANY_INDEX]);

        patients.add(patientModel);
        line = br.readLine(); //next line
      }

    } catch (IOException ioe) {
      log.error("Oopsies csv issue occurred, maybe check your path: " + EXAMPLE_CSV_PATH);
      ioe.printStackTrace();
    }
    return patients;
  }

  /**
   * Group patients in a map in association with their company and filter out duplicate ids with old version
   * @param patientModels list of all patients from the csv import
   * @return Map<String, List<PatientModel>> map of companies and their associated patients
   */
  private Map<String, List<PatientModel>> groupPatientsByCompany(List<PatientModel> patientModels) {
    Map<String, List<PatientModel>> patientCompanyMap = new HashMap<>();

    for(PatientModel patient: patientModels) {
      if(patientCompanyMap.containsKey(patient.getInsuranceCompany())) {
        List<PatientModel> companyPatientsList =
            patientCompanyMap.get(patient.getInsuranceCompany());

        PatientModel existingPatient = companyPatientsList.stream().
            filter(p -> p.getUserId().equals(patient.getUserId())).findFirst().orElse(null); //find duplicate ids

        if(existingPatient != null) {
          if(patient.getVersion().compareTo(existingPatient.getVersion()) > 0) {
            companyPatientsList.remove(existingPatient); //removing old version of duplicate record
            companyPatientsList.add(patient);
          }
          //else don't add
        } else {
          companyPatientsList.add(patient); //list is by reference no need to put in the map again
        }
      } else {
        List<PatientModel> companyPatientsList = new ArrayList<>();
        companyPatientsList.add(patient);
        patientCompanyMap.put(patient.getInsuranceCompany(), companyPatientsList); //put in map for first time
      }
    }

    return patientCompanyMap;
  }

  /**
   * Sorts patients by Last and First name within a company
   * @param companyPatients map of companies and their associated patients
   */
  private void sortByLastAndFirstName(Map<String,
      List<PatientModel>> companyPatients) {

    for (Entry<String, List<PatientModel>> company : companyPatients.entrySet()) {

      Function<PatientModel, String> byLastName = PatientModel::getLastName;
      Function<PatientModel, String> byFirstName = PatientModel::getFirstName;

      Comparator<PatientModel> lastThenFirst =
          Comparator.comparing(byLastName).thenComparing(byFirstName);

      //uses comparator to sort by last then first name
      company.setValue(company.getValue().stream()
          .sorted(lastThenFirst)
          .collect(Collectors.toList()));
    }
  }

  /**
   * Creates a csv file for each company that contains all their patient data
   * @param companyPatients map of companies and their associated patients
   */
  private void createCSVFilesForEachCompany(Map<String,
      List<PatientModel>> companyPatients) {

    for (Entry<String, List<PatientModel>> company : companyPatients.entrySet()) {
      PrintWriter pw;
      try {
        File companyFile = new File(CSV_RESULTS_PATH + company.getKey() + ".csv");
        companyFile.createNewFile();
        pw = new PrintWriter(companyFile);
        StringBuilder builder = new StringBuilder();

        for(PatientModel patient: company.getValue()) {
          builder.append(patient.getUserId());
          builder.append(',');
          builder.append(patient.getFirstName());
          builder.append(',');
          builder.append(patient.getLastName());
          builder.append(',');
          builder.append(patient.getVersion());
          builder.append(',');
          builder.append(patient.getInsuranceCompany());
          builder.append('\n');
        }
        pw.write(builder.toString());
        pw.close();
      } catch (IOException e) {
        log.error("Failed to create file name for company: " + company.getKey());
        e.printStackTrace();
      }
    }
  }
}
